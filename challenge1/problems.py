def compress(chars: str) -> str:
    """Compresses the string @chars

       Time complexity: O(n), we only pass the array once, dynamically building the result.
       Space complexity: O(1), wxcept for the resulting string, which is mandatory (it can have up to n characters)
    """
    if len(chars) == 0:
        return ""

    result = ""
    crt_char, crt_count = chars[0], 1

    for i in range(1, len(chars)):
        if chars[i] == crt_char:
            crt_count += 1
        else:
            result += crt_char

            if crt_count > 1:
                for c in str(crt_count):
                    result += c

            crt_char, crt_count = chars[i], 1

    result += crt_char
    if crt_count > 1:
        for c in str(crt_count):
            result += c

    return result


assert compress('bbcceeee') == 'b2c2e4'
assert compress('aaabbbcccaaa') == 'a3b3c3a3'
assert compress('a') == 'a'

##########################################################


class Graph:

    def __init__(self, links: list = []):
        self._adj_list = {}
        self._connections = {}
        self._routers = []
        self._max_connections = 0

        for i in range(1, len(links)):
            self._add_edge(links[i-1], links[i])

    def _add_edge(self, source, dest):
        neighbors = self._adj_list.get(source, {})
        neighbors[dest] = neighbors.get(dest, 0) + 1  # adds a new connection
        self._adj_list[source] = neighbors
        self._connections[dest] = self._connections.get(dest, 0) + 1
        self._connections[source] = self._connections.get(source, 0) + 1

        new_max_degree = max(self._max_connections,
                             self._connections[dest], self._connections[source])

        if new_max_degree > self._max_connections:
            self._routers = []
            self._max_connections = new_max_degree

        if self._connections[dest] == self._max_connections:
            self._routers.append(dest)

        if self._connections[source] == self._max_connections:
            self._routers.append(source)

    def get_routers(self) -> list:
        return self._routers


def identify_router(g: Graph) -> list:
    """Returns the native get_routers result of the graph

    O(1), since the max degree nodes (routers) are computed on the fly while
        constructing the graph
    """
    return g.get_routers()


assert identify_router(Graph([1, 2, 3, 5, 2, 1])) == [2]
assert identify_router(Graph([1, 3, 5, 6, 4, 5, 2, 6])) == [5]
assert sorted(identify_router(Graph([2, 4, 6, 2, 5, 6]))) == [2, 6]
