from django.urls import path
from .views import UserRegistrationView, UserLoginView, UserInfo

app_name = 'authentication'

urlpatterns = [
    path('register/', UserRegistrationView.as_view(), name="register"),
    path('login/', UserLoginView.as_view(), name="login"),
    path('profile/', UserInfo.as_view(), name='profile')
]
