import uuid
from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser


class UserManager(BaseUserManager):

    def create_user(self, email, password=None):
        """Create and return a `User` with an email, username and password

        Arguments:
            email {[type]} -- [description]

        Keyword Arguments:
            password {[type]} -- [description] (default: {None})
        """
        if not email:
            raise ValueError('Users must hava an email address')

        user = self.model(
            email=self.normalize_email(email)
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        """
        Create and return a `User` with superuser (admin) permissions.
        """
        if password is None:
            raise TypeError('Superusers must have a password.')

        user = self.create_user(email, password)
        user.is_superuser = True
        user.is_staff = True
        user.save()

        return user


class User(AbstractBaseUser):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    email = models.EmailField(
        verbose_name='email address',
        max_length=128,
        unique=True
    )
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    def __str__(self):
        return f"<User {self.email}>"

    class Meta:

        db_table = "login"

    def has_perm(self, perm, obj=None):
        return self.is_superuser

    def has_module_perms(self, app_label):
        return self.is_superuser


class UserProfile(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    first_name = models.CharField(max_length=32, unique=False)
    last_name = models.CharField(max_length=32, unique=False)
    phone_number = models.CharField(max_length=16, unique=True, null=False, blank=False)
    age = models.PositiveIntegerField(null=False, blank=False)

    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('O', 'Other')
    )
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)

    SUBSCRIPTION_CHOICES = (
        ('D', 'Default'),
        ('P', 'Premium')
    )
    subscription = models.CharField(max_length=1, choices=SUBSCRIPTION_CHOICES, default='D')

    class Meta:

        db_table = "profile"
