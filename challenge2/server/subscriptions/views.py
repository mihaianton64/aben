from rest_framework.generics import CreateAPIView, RetrieveAPIView
from django.conf import settings
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from rest_framework.response import Response
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
import stripe
from authentication.models import UserProfile

stripe.api_key = settings.STRIPE_TEST_SECRET_KEY


class SubscriptionView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = JSONWebTokenAuthentication

    def post(self, request):

        try:
            user_profile = UserProfile.objects.get(user=request.user)
            user_profile.subscription = 'P'
            user_profile.save(update_fields=["subscription"])
            email = user_profile.user.email
            payment_method_id = request.data['payment_method_id']

            # create the customer with the payment method received
            customer = stripe.Customer.create(
                email=email,
                payment_method=payment_method_id,
                invoice_settings={
                    'default_payment_method': payment_method_id
                })

            # automatically subscribe the user to premium
            stripe.Subscription.create(
                customer=customer,
                items=[
                    {
                        'price': settings.STRIPE_PREMIUM_PRICE_ID
                    }
                ]
            )

            status_code = status.HTTP_201_CREATED
            response = {
                'success': 'true',
                'status code': status.HTTP_201_CREATED,
                'message': 'Created stripe customer',
                'customer id': customer.id
            }
        except Exception as e:
            status_code = status.HTTP_400_BAD_REQUEST
            response = {
                'success': 'false',
                'status code': status_code,
                'message': 'Error creating stripe user',
                'error': str(e)
            }

        return Response(response, status=status_code)
