import store from '../store'
import { BASE_URL } from '../../constants'

const loadLoginData = () => {
  // console.log(
  //   'inits auth data',
  //   localStorage.getItem('logged_in'),
  //   localStorage.getItem('auth_token')
  // )
  store.dispatch({
    type: 'auth/login',
    payload: localStorage.getItem('auth_token'),
  })
  if (JSON.parse(localStorage.getItem('logged_in')) === true) {
    store.dispatch({
      type: 'auth/login',
      payload: localStorage.getItem('auth_token'),
    })
  } else {
    store.dispatch({
      type: 'auth/logout',
    })
  }
}

const fetchProfile = async (dispatch, getState) => {
  const token = store.getState().auth.token

  fetch(BASE_URL + 'authentication/profile/', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then((response) => response.json())
    .then((data) => {
      store.dispatch({
        type: 'profile/setLoggedInProfile',
        payload: data.data[0],
      })
    })
    .catch((err) => {
      store.dispatch({
        type: 'profile/eraseData',
      })
    })
}

export { loadLoginData, fetchProfile }
