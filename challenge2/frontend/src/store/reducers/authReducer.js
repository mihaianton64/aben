const initialState = {
  loggedIn: false,
  token: null,
}

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'auth/login': {
      return { loggedIn: true, token: action.payload }
    }
    case 'auth/logout': {
      return { loggedIn: false, token: null }
    }
    default:
      return state
  }
}

export default authReducer
