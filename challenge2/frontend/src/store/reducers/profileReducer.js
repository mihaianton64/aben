import { PlaylistAddOutlined } from '@material-ui/icons'

const initialState = {
  hasProfileData: false,
  profile: {},
}

const profileReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'profile/setLoggedInProfile': {
      return { hasProfileData: true, profile: action.payload }
    }
    case 'profile/eraseData': {
      return { hasProfileData: false, profile: {} }
    }
    default:
      return state
  }
}

export default profileReducer
