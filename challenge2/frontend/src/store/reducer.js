import { combineReducers } from 'redux'

import profileReducer from './reducers/profileReducer'
import authReducer from './reducers/authReducer'

const rootReducer = combineReducers({
  auth: authReducer,
  profile: profileReducer,
})

export default rootReducer
