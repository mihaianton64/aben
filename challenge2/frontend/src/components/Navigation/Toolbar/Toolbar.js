import React from 'react'

import NavigationItems from '../NavigationItems/NavigationItems'
import Logo from '../../Logo/Logo'
import classes from './Toolbar.module.css'
import InfoBox from '../InfoBox/InfoBox'
import locationPin from '../../../assets/icons/locationPin.png'
import store from '../../../store/store'
import { useSelector } from 'react-redux'

const selectLoggedIn = (state) => state.auth.loggedIn
const selectMembershipType = (state) => state.profile.profile['subscription']

const parseSubscriptionMode = (mode) => {
  // console.log(mode)
  if (mode === 'D') {
    return 'Default membership'
  } else if (mode === 'P') {
    return 'Premium membership'
  }
  return null
}

const Toolbar = (props) => {
  const loggedIn = useSelector(selectLoggedIn)
  const memberType = useSelector(selectMembershipType)

  return (
    <header className={classes.Toolbar}>
      <div className={classes.LogoInfoBox}>
        <Logo height="64px" />
        <div className={classes.InfoBox}>
          <InfoBox>
            <h2>Aben</h2>

            <p>{parseSubscriptionMode(memberType)}</p>
          </InfoBox>
        </div>
      </div>
      <nav>
        <NavigationItems />
      </nav>
    </header>
  )
}

export default Toolbar
