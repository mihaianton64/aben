import React, { useEffect, useState } from 'react'

import NavigationItem from './NavigationItem/NavigationItem'
import classes from './NavigationItems.module.css'
import store from '../../../store/store'
import { useSelector } from 'react-redux'

const signOut = () => {
  localStorage.setItem('logged_in', false)
  localStorage.setItem('auth_token', '')
  store.dispatch({ type: 'auth/logout' })
}

const selectLoggedIn = (state) => state.auth.loggedIn
const selectProfileUpdated = (state) => state.profile.hasProfileData

const NavigationItems = () => {
  // const [loggedIn, setLoggedIn] = useState(localStorage.getItem('logged_in'))

  const loggedIn = useSelector(selectLoggedIn)
  const hasProfile = useSelector(selectProfileUpdated)

  return (
    <ul className={classes.NavigationItems}>
      {loggedIn && hasProfile ? (
        <p>Hi, {store.getState().profile.profile['first_name']}</p>
      ) : null}
      {loggedIn ? (
        <NavigationItem onClick={signOut} link={'/'}>
          Sign Out
        </NavigationItem>
      ) : null}
      {loggedIn ? null : <NavigationItem link="/login">Login</NavigationItem>}
      {loggedIn ? null : (
        <NavigationItem link="/register">Register</NavigationItem>
      )}
    </ul>
  )
}

export default NavigationItems
