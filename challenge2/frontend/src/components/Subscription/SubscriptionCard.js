import React from 'react'
import Card from '@material-ui/core/Card'
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'

const useStyles = makeStyles({
  root: {
    width: 220,
    height: 300,
    background: 'linear-gradient(to right bottom, #20c7c8, #37a0dc)',
    padding: 20,
  },
  subscriptionType: { color: 'white', fontSize: 28, marginBottom: 7 },
  price: { color: '#f0f8ff', fontSize: 20 },
  media: {
    height: 140,
  },
  grid: {
    height: '100%',
  },
  fullWidth: {
    width: '100%',
  },
  separator: {
    backgroundColor: '#d3d3d3',
    color: '#d3d3d3',
    height: 1,
    border: 0,
    borderRadius: 10,
  },
})

const SubscriptonCard = (props) => {
  const classes = useStyles()

  return (
    <Card className={classes.root}>
      <Grid
        className={classes.grid}
        container
        direction="column"
        justify="space-between"
        alignItems="flex-start"
      >
        <Grid item className={classes.fullWidth}>
          <h3 className={classes.subscriptionType}>PREMIUM</h3>
        </Grid>
        <Grid item className={classes.fullWidth}>
          <h4 className={classes.price}>{props.priceText}</h4>
        </Grid>
        <Grid item className={classes.fullWidth}>
          <hr className={classes.separator} />
        </Grid>
        <Grid item>
          <ul>
            <li>Cool feature</li>
            <li>Cool feature</li>
            <li>Cool feature</li>
          </ul>
        </Grid>

        <Grid item className={classes.fullWidth}>
          <Button onClick={props.onClick} variant="contained">
            Get it now
          </Button>
        </Grid>
      </Grid>
    </Card>
  )
}

export default SubscriptonCard
