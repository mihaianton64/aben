import React, { useState } from 'react'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import { makeStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import PersonIcon from '@material-ui/icons/Person'
import logo from '../../../assets/icons/logo-dark.svg'
import store from '../../../store/store'
import { fetchProfile } from '../../../store/api/authAPI'
import { useHistory } from 'react-router-dom'
import { BASE_URL } from '../../../constants'

import classes from './LoginBox.module.css'

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(2),
      width: '25ch',
    },
  },
}))

const setAuthData = (loggedIn, token) => {
  if (loggedIn) {
    localStorage.setItem('logged_in', true)
    localStorage.setItem('auth_token', token)
    store.dispatch({
      type: 'auth/login',
      payload: token,
    })
    store.dispatch(fetchProfile)
  } else {
    localStorage.setItem('logged_in', false)
    localStorage.setItem('auth_token', '')
    store.dispatch({ type: 'auth/logout' })
  }
}

const LoginBox = (props) => {
  const materialClasses = useStyles()
  const history = useHistory()

  const [password, setPassword] = useState('')
  const [email, setEmail] = useState('')

  const handleLogin = () => {
    const body = {
      email: email,
      password: password,
    }

    setPassword('')
    setEmail('')

    fetch(BASE_URL + 'authentication/login/', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(body),
    })
      .then((response) => response.json())
      .then((data) => {
        setAuthData(true, data.token)
        history.push('/')
      })
      .catch((err) => {
        setAuthData(false, '')
      })
    // setAuthData(true, 'abc123')
  }
  return (
    <div className={classes.LoginBox}>
      <Grid container direction="row" justify="center" alignItems="center">
        <Grid item xs={6}>
          <form className={materialClasses.root} noValidate autoComplete="off">
            <div>
              <TextField
                id="email"
                label="Email"
                value={email}
                onChange={(e) => {
                  setEmail(e.target.value)
                }}
              />
              <TextField
                id="password"
                label="Password"
                type="password"
                autoComplete="current-password"
                value={password}
                onChange={(e) => {
                  setPassword(e.target.value)
                }}
              />
            </div>
          </form>
        </Grid>
        <Grid item xs={6}>
          <div>
            <Grid
              container
              spacing={6}
              direction="column"
              justify="space-between"
              alignItems="center"
            >
              <Grid item xs={12}>
                <img style={{ width: '200px' }} src={logo} alt={'Logo'} />
              </Grid>
              <Grid item xs={12}>
                <Button
                  variant="contained"
                  className={classes.button}
                  style={{ backgroundColor: '#81a5c9' }}
                  endIcon={<PersonIcon />}
                  onClick={handleLogin}
                >
                  Login
                </Button>
              </Grid>
            </Grid>
          </div>
        </Grid>
      </Grid>
    </div>
  )
}

export default LoginBox
