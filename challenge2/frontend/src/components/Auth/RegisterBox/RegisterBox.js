import React, { useState } from 'react'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import { makeStyles } from '@material-ui/core/styles'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '@material-ui/core/Button'
import PersonIcon from '@material-ui/icons/Person'
import logo from '../../../assets/icons/logo-dark.svg'
import { useHistory } from 'react-router-dom'
import { BASE_URL } from '../../../constants'
import classes from './RegisterBox.module.css'

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(2),
      width: '25ch',
    },
  },
}))

const RegisterBox = (props) => {
  const materialClasses = useStyles()
  const history = useHistory()

  const [gender, setGender] = useState('')
  const [password, setPassword] = useState('')
  const [email, setEmail] = useState('')
  const [name, setName] = useState('')
  const [surname, setSurname] = useState('')
  const [phonenr, setPhonenr] = useState('')
  const [age, setAge] = useState('')

  const genders = [
    {
      value: 'M',
      label: 'Male',
    },
    {
      value: 'F',
      label: 'Female',
    },
    {
      value: 'O',
      label: 'Other',
    },
  ]
  const handleGenderChange = (event) => {
    setGender(event.target.value)
  }

  const handleRegister = () => {
    const body = {
      email: email,
      password: password,
      profile: {
        first_name: surname,
        last_name: name,
        phone_number: phonenr,
        age: parseInt(age),
        gender: gender,
      },
    }
    // console.log(body)
    setGender('')
    setPassword('')
    setEmail('')
    setName('')
    setSurname('')
    setPhonenr('')
    setAge('')

    fetch(BASE_URL + 'authentication/register/', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(body),
    })
      .then((response) => response.json())
      .then((data) => history.push('/login'))
      .catch((err) => null)
  }

  return (
    <div className={classes.RegisterBox}>
      <Grid container direction="row" justify="center" alignItems="center">
        <Grid item xs={6}>
          <form className={materialClasses.root} noValidate autoComplete="off">
            <div>
              <TextField
                id="email"
                label="Email"
                value={email}
                onChange={(e) => {
                  setEmail(e.target.value)
                }}
              />
              <TextField
                id="password"
                label="Password"
                type="password"
                autoComplete="current-password"
                value={password}
                onChange={(e) => {
                  setPassword(e.target.value)
                }}
              />
              <TextField
                id="name"
                label="Name"
                value={name}
                onChange={(e) => {
                  setName(e.target.value)
                }}
              />
              <TextField
                id="surname"
                label="Surname"
                value={surname}
                onChange={(e) => {
                  setSurname(e.target.value)
                }}
              />
              <TextField
                id="phone-number"
                label="Phone number"
                value={phonenr}
                onChange={(e) => {
                  setPhonenr(e.target.value)
                }}
              />
              <TextField
                id="age"
                label="Age"
                type="number"
                value={age}
                onChange={(e) => {
                  setAge(e.target.value)
                }}
              />
              <TextField
                id="standard-select-currency"
                select
                label="Gender"
                value={gender}
                onChange={handleGenderChange}
              >
                {genders.map((option) => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
              </TextField>
            </div>
          </form>
        </Grid>
        <Grid item xs={6}>
          <div>
            <Grid
              container
              spacing={6}
              direction="column"
              justify="space-between"
              alignItems="center"
            >
              <Grid item xs={12}>
                <img style={{ width: '200px' }} src={logo} alt={'Logo'} />
              </Grid>
              <Grid item xs={12}>
                <Button
                  variant="contained"
                  className={classes.button}
                  style={{ backgroundColor: '#81a5c9' }}
                  endIcon={<PersonIcon />}
                  onClick={handleRegister}
                >
                  Register
                </Button>
              </Grid>
            </Grid>
          </div>
        </Grid>
      </Grid>
    </div>
  )
}

export default RegisterBox
