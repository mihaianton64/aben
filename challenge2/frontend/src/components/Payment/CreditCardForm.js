import React, { useState } from 'react'
import Cards from 'react-credit-cards'
import 'react-credit-cards/es/styles-compiled.css'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import { makeStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import { useStripe, useElements, CardElement } from '@stripe/react-stripe-js'
import { loadStripe } from '@stripe/stripe-js'
import './Styles.css'
import { BASE_URL } from '../../constants'
import store from '../../store/store'
import { useHistory } from 'react-router-dom'

const CARD_ELEMENT_OPTIONS = {
  style: {
    base: {
      color: '#32325d',
      fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
      fontSmoothing: 'antialiased',
      fontSize: '16px',
      '::placeholder': {
        color: '#aab7c4',
      },
    },
    invalid: {
      color: '#fa755a',
      iconColor: '#fa755a',
    },
  },
}

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '25ch',
    },
    display: 'flex',
    flexWrap: 'wrap',
    maxWidth: 700,
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: '25ch',
  },
  button: {
    marginTop: 20,
  },
}))

const CreditCardForm = (props) => {
  const stripe = useStripe()
  const elements = useElements()
  const history = useHistory()

  const classes = useStyles()

  const stripeTokenHandler = async (id) => {
    const paymentData = { payment_method_id: id }
    const authToken = store.getState().auth.token
    // console.log(paymentData)

    const response = await fetch(BASE_URL + 'subscription/subscribe/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${authToken}`,
      },
      body: JSON.stringify(paymentData),
    })
    history.push('/')

    // Return and display the result of the charge.
    // console.log(response.json())
  }

  const submitPayment = async (event) => {
    event.preventDefault()

    if (!stripe || !elements) {
      return
    }

    const card = elements.getElement(CardElement)
    const { error, paymentMethod } = await stripe.createPaymentMethod({
      type: 'card',
      card: card,
    })

    // console.log(paymentMethod)

    if (error) {
      return
    } else {
      stripeTokenHandler(paymentMethod.id)
    }
  }

  return (
    <Grid
      className={classes.root}
      container
      direction="row"
      justify="center"
      alignItems="center"
      spacing={0}
    >
      <Grid
        className={classes.root}
        container
        direction="column"
        justify="center"
        alignItems="center"
        spacing={1}
      >
        <Grid item xs={6}>
          <CardElement options={CARD_ELEMENT_OPTIONS} />
        </Grid>
      </Grid>
      <Button
        disabled={!stripe}
        onClick={(e) => submitPayment(e)}
        className={classes.button}
        variant="contained"
      >
        Subscribe
      </Button>
    </Grid>
  )
}

export default CreditCardForm
