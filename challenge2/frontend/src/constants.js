//api constants

const DEBUG_MODE = false

const WEBSITE_NAME = 'Website name'

const BASE_URL = '<prod url here>'
// const BASE_URL = 'http://127.0.0.1:8000/'

export { DEBUG_MODE, WEBSITE_NAME, BASE_URL }
