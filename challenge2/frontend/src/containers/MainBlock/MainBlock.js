import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom'

import HomePage from './HomePage/HomePage'
import BlankPage from './BlankPage/BlankPage'

import classes from './MainBlock.module.css'

import NotFoundPage from './NotFoundPage/NotFoundPage'
import LoginPage from './Auth/LoginPage/LoginPage'
import RegisterPage from './Auth/RegisterPage/RegisterPage'

class MainBlock extends Component {
  render() {
    return (
      <div className={classes.MainBlock}>
        <Switch>
          <Route path="/" exact component={HomePage} />
          <Route path="/register" component={RegisterPage} />
          <Route path="/login" component={LoginPage} />

          <Route path="/blank" component={BlankPage} />

          <Route component={NotFoundPage} />
        </Switch>
      </div>
    )
  }
}

export default MainBlock
