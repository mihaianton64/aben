import React, { useEffect, useState } from 'react'

import classes from './HomePage.module.css'
import Text from '../BlogPage/Elements/Text/Text'
import Title from '../BlogPage/Elements/Title/Title'
import Subtitle from '../BlogPage/Elements/Subtitle/Subtitle'
import { WEBSITE_NAME, BASE_URL } from '../../../constants'
import SubscriptonCard from '../../../components/Subscription/SubscriptionCard'
import CreditCardForm from '../../../components/Payment/CreditCardForm'
import { useSelector } from 'react-redux'
import store from '../../../store/store'

const selectLoggedIn = (state) => state.auth.loggedIn
const selectSubscriptionType = (state) => state.profile.profile['subscription']

const HomePage = (props) => {
  useEffect(() => {
    document.title = 'Home | ' + WEBSITE_NAME
  }, [])

  const [inPayment, setInpayment] = useState(false)
  const loggedIn = useSelector(selectLoggedIn)
  const subscriptionType = useSelector(selectSubscriptionType)

  const onPaymentConfirmationClick = (cardNumber, name, expiry, cvc) => {
    setInpayment(false)
    // console.log(cardNumber, name, expiry, cvc)
  }

  return (
    <div className={classes.HomePage}>
      <Title>Aben</Title>
      {!loggedIn ? <Subtitle>Login to begin.</Subtitle> : null}

      {loggedIn && !inPayment && subscriptionType !== 'P' ? (
        <SubscriptonCard
          onClick={() => setInpayment(true)}
          priceText={'€7.99 / month'}
        />
      ) : null}

      {loggedIn && inPayment ? (
        <CreditCardForm onClick={onPaymentConfirmationClick} />
      ) : null}
    </div>
  )
}

export default HomePage
