import React from 'react'

import classes from './RegisterPage.module.css'
import RegisterBox from '../../../../components/Auth/RegisterBox/RegisterBox'

const RegisterPage = (props) => {
  return (
    <div className={classes.RegisterPage}>
      <RegisterBox />
    </div>
  )
}

export default RegisterPage
