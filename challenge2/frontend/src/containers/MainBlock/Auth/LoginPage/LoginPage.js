import React from 'react'
import LoginBox from '../../../../components/Auth/LoginBox/LoginBox'

import classes from './LoginPage.module.css'

const LoginPage = (props) => {
  return (
    <div className={classes.LoginPage}>
      <LoginBox />
    </div>
  )
}

export default LoginPage
