import React, { Component } from "react";

import Footer from "../../components/Footer/Footer";
import Toolbar from "../../components/Navigation/Toolbar/Toolbar";
import classes from "./Layout.module.css";

class Layout extends Component {
  render() {
    return (
      <div className={classes.LayoutBox}>
        <Toolbar />

        <main className={classes.ContentBox}>{this.props.children}</main>

        <div className={classes.FooterBox}>
          <Footer text="Mihai Anton © | 2020" />
        </div>
      </div>
    );
  }
}

export default Layout;
